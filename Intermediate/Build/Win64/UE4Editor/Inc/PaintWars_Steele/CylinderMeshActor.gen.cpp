// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWars_Steele/CylinderMeshActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCylinderMeshActor() {}
// Cross Module References
	PAINTWARS_STEELE_API UClass* Z_Construct_UClass_ACylinderMeshActor_NoRegister();
	PAINTWARS_STEELE_API UClass* Z_Construct_UClass_ACylinderMeshActor();
	RUNTIMEMESHCOMPONENT_API UClass* Z_Construct_UClass_ARuntimeMeshActor();
	UPackage* Z_Construct_UPackage__Script_PaintWars_Steele();
// End Cross Module References
	void ACylinderMeshActor::StaticRegisterNativesACylinderMeshActor()
	{
	}
	UClass* Z_Construct_UClass_ACylinderMeshActor_NoRegister()
	{
		return ACylinderMeshActor::StaticClass();
	}
	struct Z_Construct_UClass_ACylinderMeshActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACylinderMeshActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ARuntimeMeshActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWars_Steele,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACylinderMeshActor_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Input" },
		{ "IncludePath", "CylinderMeshActor.h" },
		{ "ModuleRelativePath", "CylinderMeshActor.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACylinderMeshActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACylinderMeshActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACylinderMeshActor_Statics::ClassParams = {
		&ACylinderMeshActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ACylinderMeshActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ACylinderMeshActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACylinderMeshActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACylinderMeshActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACylinderMeshActor, 877420040);
	template<> PAINTWARS_STEELE_API UClass* StaticClass<ACylinderMeshActor>()
	{
		return ACylinderMeshActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACylinderMeshActor(Z_Construct_UClass_ACylinderMeshActor, &ACylinderMeshActor::StaticClass, TEXT("/Script/PaintWars_Steele"), TEXT("ACylinderMeshActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACylinderMeshActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
