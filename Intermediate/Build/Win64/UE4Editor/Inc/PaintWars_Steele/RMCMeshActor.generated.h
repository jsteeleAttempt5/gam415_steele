// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWARS_STEELE_RMCMeshActor_generated_h
#error "RMCMeshActor.generated.h already included, missing '#pragma once' in RMCMeshActor.h"
#endif
#define PAINTWARS_STEELE_RMCMeshActor_generated_h

#define PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_SPARSE_DATA
#define PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_RPC_WRAPPERS
#define PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARMCMeshActor(); \
	friend struct Z_Construct_UClass_ARMCMeshActor_Statics; \
public: \
	DECLARE_CLASS(ARMCMeshActor, ARuntimeMeshActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWars_Steele"), NO_API) \
	DECLARE_SERIALIZER(ARMCMeshActor)


#define PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_INCLASS \
private: \
	static void StaticRegisterNativesARMCMeshActor(); \
	friend struct Z_Construct_UClass_ARMCMeshActor_Statics; \
public: \
	DECLARE_CLASS(ARMCMeshActor, ARuntimeMeshActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWars_Steele"), NO_API) \
	DECLARE_SERIALIZER(ARMCMeshActor)


#define PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARMCMeshActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARMCMeshActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARMCMeshActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARMCMeshActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARMCMeshActor(ARMCMeshActor&&); \
	NO_API ARMCMeshActor(const ARMCMeshActor&); \
public:


#define PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARMCMeshActor(ARMCMeshActor&&); \
	NO_API ARMCMeshActor(const ARMCMeshActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARMCMeshActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARMCMeshActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARMCMeshActor)


#define PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_PRIVATE_PROPERTY_OFFSET
#define PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_8_PROLOG
#define PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_PRIVATE_PROPERTY_OFFSET \
	PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_SPARSE_DATA \
	PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_RPC_WRAPPERS \
	PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_INCLASS \
	PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_PRIVATE_PROPERTY_OFFSET \
	PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_SPARSE_DATA \
	PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_INCLASS_NO_PURE_DECLS \
	PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWARS_STEELE_API UClass* StaticClass<class ARMCMeshActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWars_Steele_Source_PaintWars_Steele_RMCMeshActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
