// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWARS_STEELE_PaintWars_SteeleGameMode_generated_h
#error "PaintWars_SteeleGameMode.generated.h already included, missing '#pragma once' in PaintWars_SteeleGameMode.h"
#endif
#define PAINTWARS_STEELE_PaintWars_SteeleGameMode_generated_h

#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_SPARSE_DATA
#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_RPC_WRAPPERS
#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintWars_SteeleGameMode(); \
	friend struct Z_Construct_UClass_APaintWars_SteeleGameMode_Statics; \
public: \
	DECLARE_CLASS(APaintWars_SteeleGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWars_Steele"), PAINTWARS_STEELE_API) \
	DECLARE_SERIALIZER(APaintWars_SteeleGameMode)


#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaintWars_SteeleGameMode(); \
	friend struct Z_Construct_UClass_APaintWars_SteeleGameMode_Statics; \
public: \
	DECLARE_CLASS(APaintWars_SteeleGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWars_Steele"), PAINTWARS_STEELE_API) \
	DECLARE_SERIALIZER(APaintWars_SteeleGameMode)


#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PAINTWARS_STEELE_API APaintWars_SteeleGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintWars_SteeleGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PAINTWARS_STEELE_API, APaintWars_SteeleGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintWars_SteeleGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PAINTWARS_STEELE_API APaintWars_SteeleGameMode(APaintWars_SteeleGameMode&&); \
	PAINTWARS_STEELE_API APaintWars_SteeleGameMode(const APaintWars_SteeleGameMode&); \
public:


#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PAINTWARS_STEELE_API APaintWars_SteeleGameMode(APaintWars_SteeleGameMode&&); \
	PAINTWARS_STEELE_API APaintWars_SteeleGameMode(const APaintWars_SteeleGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PAINTWARS_STEELE_API, APaintWars_SteeleGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintWars_SteeleGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintWars_SteeleGameMode)


#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_9_PROLOG
#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_SPARSE_DATA \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_RPC_WRAPPERS \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_INCLASS \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_SPARSE_DATA \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_INCLASS_NO_PURE_DECLS \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWARS_STEELE_API UClass* StaticClass<class APaintWars_SteeleGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
