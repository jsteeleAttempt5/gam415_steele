// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWARS_STEELE_RuntimeCubeActor_generated_h
#error "RuntimeCubeActor.generated.h already included, missing '#pragma once' in RuntimeCubeActor.h"
#endif
#define PAINTWARS_STEELE_RuntimeCubeActor_generated_h

#define PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_SPARSE_DATA
#define PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_RPC_WRAPPERS
#define PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARuntimeCubeActor(); \
	friend struct Z_Construct_UClass_ARuntimeCubeActor_Statics; \
public: \
	DECLARE_CLASS(ARuntimeCubeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWars_Steele"), NO_API) \
	DECLARE_SERIALIZER(ARuntimeCubeActor)


#define PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesARuntimeCubeActor(); \
	friend struct Z_Construct_UClass_ARuntimeCubeActor_Statics; \
public: \
	DECLARE_CLASS(ARuntimeCubeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWars_Steele"), NO_API) \
	DECLARE_SERIALIZER(ARuntimeCubeActor)


#define PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARuntimeCubeActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARuntimeCubeActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARuntimeCubeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARuntimeCubeActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARuntimeCubeActor(ARuntimeCubeActor&&); \
	NO_API ARuntimeCubeActor(const ARuntimeCubeActor&); \
public:


#define PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARuntimeCubeActor(ARuntimeCubeActor&&); \
	NO_API ARuntimeCubeActor(const ARuntimeCubeActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARuntimeCubeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARuntimeCubeActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARuntimeCubeActor)


#define PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_PRIVATE_PROPERTY_OFFSET
#define PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_9_PROLOG
#define PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_SPARSE_DATA \
	PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_RPC_WRAPPERS \
	PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_INCLASS \
	PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_PRIVATE_PROPERTY_OFFSET \
	PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_SPARSE_DATA \
	PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_INCLASS_NO_PURE_DECLS \
	PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWARS_STEELE_API UClass* StaticClass<class ARuntimeCubeActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWars_Steele_Source_PaintWars_Steele_RuntimeCubeActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
