// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWars_Steele/PaintWars_SteeleHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaintWars_SteeleHUD() {}
// Cross Module References
	PAINTWARS_STEELE_API UClass* Z_Construct_UClass_APaintWars_SteeleHUD_NoRegister();
	PAINTWARS_STEELE_API UClass* Z_Construct_UClass_APaintWars_SteeleHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_PaintWars_Steele();
// End Cross Module References
	void APaintWars_SteeleHUD::StaticRegisterNativesAPaintWars_SteeleHUD()
	{
	}
	UClass* Z_Construct_UClass_APaintWars_SteeleHUD_NoRegister()
	{
		return APaintWars_SteeleHUD::StaticClass();
	}
	struct Z_Construct_UClass_APaintWars_SteeleHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APaintWars_SteeleHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWars_Steele,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APaintWars_SteeleHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "PaintWars_SteeleHUD.h" },
		{ "ModuleRelativePath", "PaintWars_SteeleHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APaintWars_SteeleHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APaintWars_SteeleHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APaintWars_SteeleHUD_Statics::ClassParams = {
		&APaintWars_SteeleHUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_APaintWars_SteeleHUD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APaintWars_SteeleHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APaintWars_SteeleHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APaintWars_SteeleHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APaintWars_SteeleHUD, 454602106);
	template<> PAINTWARS_STEELE_API UClass* StaticClass<APaintWars_SteeleHUD>()
	{
		return APaintWars_SteeleHUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APaintWars_SteeleHUD(Z_Construct_UClass_APaintWars_SteeleHUD, &APaintWars_SteeleHUD::StaticClass, TEXT("/Script/PaintWars_Steele"), TEXT("APaintWars_SteeleHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APaintWars_SteeleHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
