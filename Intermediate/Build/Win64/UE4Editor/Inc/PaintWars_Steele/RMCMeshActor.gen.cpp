// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWars_Steele/RMCMeshActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRMCMeshActor() {}
// Cross Module References
	PAINTWARS_STEELE_API UClass* Z_Construct_UClass_ARMCMeshActor_NoRegister();
	PAINTWARS_STEELE_API UClass* Z_Construct_UClass_ARMCMeshActor();
	RUNTIMEMESHCOMPONENT_API UClass* Z_Construct_UClass_ARuntimeMeshActor();
	UPackage* Z_Construct_UPackage__Script_PaintWars_Steele();
// End Cross Module References
	void ARMCMeshActor::StaticRegisterNativesARMCMeshActor()
	{
	}
	UClass* Z_Construct_UClass_ARMCMeshActor_NoRegister()
	{
		return ARMCMeshActor::StaticClass();
	}
	struct Z_Construct_UClass_ARMCMeshActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARMCMeshActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ARuntimeMeshActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWars_Steele,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARMCMeshActor_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Input" },
		{ "IncludePath", "RMCMeshActor.h" },
		{ "ModuleRelativePath", "RMCMeshActor.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARMCMeshActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARMCMeshActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARMCMeshActor_Statics::ClassParams = {
		&ARMCMeshActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ARMCMeshActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ARMCMeshActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARMCMeshActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARMCMeshActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARMCMeshActor, 4069837687);
	template<> PAINTWARS_STEELE_API UClass* StaticClass<ARMCMeshActor>()
	{
		return ARMCMeshActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARMCMeshActor(Z_Construct_UClass_ARMCMeshActor, &ARMCMeshActor::StaticClass, TEXT("/Script/PaintWars_Steele"), TEXT("ARMCMeshActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARMCMeshActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
