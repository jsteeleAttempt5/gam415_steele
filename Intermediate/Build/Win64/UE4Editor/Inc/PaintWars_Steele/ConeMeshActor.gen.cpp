// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWars_Steele/ConeMeshActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConeMeshActor() {}
// Cross Module References
	PAINTWARS_STEELE_API UClass* Z_Construct_UClass_AConeMeshActor_NoRegister();
	PAINTWARS_STEELE_API UClass* Z_Construct_UClass_AConeMeshActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_PaintWars_Steele();
// End Cross Module References
	void AConeMeshActor::StaticRegisterNativesAConeMeshActor()
	{
	}
	UClass* Z_Construct_UClass_AConeMeshActor_NoRegister()
	{
		return AConeMeshActor::StaticClass();
	}
	struct Z_Construct_UClass_AConeMeshActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AConeMeshActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWars_Steele,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConeMeshActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ConeMeshActor.h" },
		{ "ModuleRelativePath", "ConeMeshActor.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AConeMeshActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AConeMeshActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AConeMeshActor_Statics::ClassParams = {
		&AConeMeshActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AConeMeshActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AConeMeshActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AConeMeshActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AConeMeshActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AConeMeshActor, 519812024);
	template<> PAINTWARS_STEELE_API UClass* StaticClass<AConeMeshActor>()
	{
		return AConeMeshActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AConeMeshActor(Z_Construct_UClass_AConeMeshActor, &AConeMeshActor::StaticClass, TEXT("/Script/PaintWars_Steele"), TEXT("AConeMeshActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AConeMeshActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
