// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWARS_STEELE_PaintWars_SteeleCharacter_generated_h
#error "PaintWars_SteeleCharacter.generated.h already included, missing '#pragma once' in PaintWars_SteeleCharacter.h"
#endif
#define PAINTWARS_STEELE_PaintWars_SteeleCharacter_generated_h

#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_SPARSE_DATA
#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_RPC_WRAPPERS
#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintWars_SteeleCharacter(); \
	friend struct Z_Construct_UClass_APaintWars_SteeleCharacter_Statics; \
public: \
	DECLARE_CLASS(APaintWars_SteeleCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWars_Steele"), NO_API) \
	DECLARE_SERIALIZER(APaintWars_SteeleCharacter)


#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAPaintWars_SteeleCharacter(); \
	friend struct Z_Construct_UClass_APaintWars_SteeleCharacter_Statics; \
public: \
	DECLARE_CLASS(APaintWars_SteeleCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWars_Steele"), NO_API) \
	DECLARE_SERIALIZER(APaintWars_SteeleCharacter)


#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintWars_SteeleCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintWars_SteeleCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintWars_SteeleCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintWars_SteeleCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintWars_SteeleCharacter(APaintWars_SteeleCharacter&&); \
	NO_API APaintWars_SteeleCharacter(const APaintWars_SteeleCharacter&); \
public:


#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintWars_SteeleCharacter(APaintWars_SteeleCharacter&&); \
	NO_API APaintWars_SteeleCharacter(const APaintWars_SteeleCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintWars_SteeleCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintWars_SteeleCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintWars_SteeleCharacter)


#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(APaintWars_SteeleCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(APaintWars_SteeleCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(APaintWars_SteeleCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(APaintWars_SteeleCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(APaintWars_SteeleCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(APaintWars_SteeleCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(APaintWars_SteeleCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(APaintWars_SteeleCharacter, L_MotionController); }


#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_11_PROLOG
#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_SPARSE_DATA \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_RPC_WRAPPERS \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_INCLASS \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_SPARSE_DATA \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_INCLASS_NO_PURE_DECLS \
	PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWARS_STEELE_API UClass* StaticClass<class APaintWars_SteeleCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWars_Steele_Source_PaintWars_Steele_PaintWars_SteeleCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
