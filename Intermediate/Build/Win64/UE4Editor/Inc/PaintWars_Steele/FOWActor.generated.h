// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector2D;
#ifdef PAINTWARS_STEELE_FOWActor_generated_h
#error "FOWActor.generated.h already included, missing '#pragma once' in FOWActor.h"
#endif
#define PAINTWARS_STEELE_FOWActor_generated_h

#define PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_SPARSE_DATA
#define PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execrevealSmoothCircle) \
	{ \
		P_GET_STRUCT_REF(FVector2D,Z_Param_Out_pos); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_radius); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->revealSmoothCircle(Z_Param_Out_pos,Z_Param_radius); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetSize) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->getSize(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetSize) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_s); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setSize(Z_Param_s); \
		P_NATIVE_END; \
	}


#define PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execrevealSmoothCircle) \
	{ \
		P_GET_STRUCT_REF(FVector2D,Z_Param_Out_pos); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_radius); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->revealSmoothCircle(Z_Param_Out_pos,Z_Param_radius); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetSize) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->getSize(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetSize) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_s); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setSize(Z_Param_s); \
		P_NATIVE_END; \
	}


#define PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFOWActor(); \
	friend struct Z_Construct_UClass_AFOWActor_Statics; \
public: \
	DECLARE_CLASS(AFOWActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWars_Steele"), NO_API) \
	DECLARE_SERIALIZER(AFOWActor)


#define PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAFOWActor(); \
	friend struct Z_Construct_UClass_AFOWActor_Statics; \
public: \
	DECLARE_CLASS(AFOWActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWars_Steele"), NO_API) \
	DECLARE_SERIALIZER(AFOWActor)


#define PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFOWActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFOWActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFOWActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFOWActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFOWActor(AFOWActor&&); \
	NO_API AFOWActor(const AFOWActor&); \
public:


#define PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFOWActor(AFOWActor&&); \
	NO_API AFOWActor(const AFOWActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFOWActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFOWActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFOWActor)


#define PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__m_squarePlane() { return STRUCT_OFFSET(AFOWActor, m_squarePlane); } \
	FORCEINLINE static uint32 __PPO__m_dynamicTexture() { return STRUCT_OFFSET(AFOWActor, m_dynamicTexture); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterial() { return STRUCT_OFFSET(AFOWActor, m_dynamicMaterial); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterialInstance() { return STRUCT_OFFSET(AFOWActor, m_dynamicMaterialInstance); }


#define PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_10_PROLOG
#define PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_PRIVATE_PROPERTY_OFFSET \
	PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_SPARSE_DATA \
	PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_RPC_WRAPPERS \
	PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_INCLASS \
	PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_PRIVATE_PROPERTY_OFFSET \
	PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_SPARSE_DATA \
	PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_INCLASS_NO_PURE_DECLS \
	PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWARS_STEELE_API UClass* StaticClass<class AFOWActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWars_Steele_Source_PaintWars_Steele_FOWActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
