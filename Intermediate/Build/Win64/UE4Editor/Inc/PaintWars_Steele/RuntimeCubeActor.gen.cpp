// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWars_Steele/RuntimeCubeActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRuntimeCubeActor() {}
// Cross Module References
	PAINTWARS_STEELE_API UClass* Z_Construct_UClass_ARuntimeCubeActor_NoRegister();
	PAINTWARS_STEELE_API UClass* Z_Construct_UClass_ARuntimeCubeActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_PaintWars_Steele();
// End Cross Module References
	void ARuntimeCubeActor::StaticRegisterNativesARuntimeCubeActor()
	{
	}
	UClass* Z_Construct_UClass_ARuntimeCubeActor_NoRegister()
	{
		return ARuntimeCubeActor::StaticClass();
	}
	struct Z_Construct_UClass_ARuntimeCubeActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARuntimeCubeActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWars_Steele,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARuntimeCubeActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "RuntimeCubeActor.h" },
		{ "ModuleRelativePath", "RuntimeCubeActor.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARuntimeCubeActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARuntimeCubeActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARuntimeCubeActor_Statics::ClassParams = {
		&ARuntimeCubeActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ARuntimeCubeActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ARuntimeCubeActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARuntimeCubeActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARuntimeCubeActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARuntimeCubeActor, 796133762);
	template<> PAINTWARS_STEELE_API UClass* StaticClass<ARuntimeCubeActor>()
	{
		return ARuntimeCubeActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARuntimeCubeActor(Z_Construct_UClass_ARuntimeCubeActor, &ARuntimeCubeActor::StaticClass, TEXT("/Script/PaintWars_Steele"), TEXT("ARuntimeCubeActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARuntimeCubeActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
