// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintWars_Steele/PaintWars_SteeleGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaintWars_SteeleGameMode() {}
// Cross Module References
	PAINTWARS_STEELE_API UClass* Z_Construct_UClass_APaintWars_SteeleGameMode_NoRegister();
	PAINTWARS_STEELE_API UClass* Z_Construct_UClass_APaintWars_SteeleGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_PaintWars_Steele();
// End Cross Module References
	void APaintWars_SteeleGameMode::StaticRegisterNativesAPaintWars_SteeleGameMode()
	{
	}
	UClass* Z_Construct_UClass_APaintWars_SteeleGameMode_NoRegister()
	{
		return APaintWars_SteeleGameMode::StaticClass();
	}
	struct Z_Construct_UClass_APaintWars_SteeleGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APaintWars_SteeleGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintWars_Steele,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APaintWars_SteeleGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "PaintWars_SteeleGameMode.h" },
		{ "ModuleRelativePath", "PaintWars_SteeleGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APaintWars_SteeleGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APaintWars_SteeleGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APaintWars_SteeleGameMode_Statics::ClassParams = {
		&APaintWars_SteeleGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_APaintWars_SteeleGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APaintWars_SteeleGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APaintWars_SteeleGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APaintWars_SteeleGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APaintWars_SteeleGameMode, 2170994540);
	template<> PAINTWARS_STEELE_API UClass* StaticClass<APaintWars_SteeleGameMode>()
	{
		return APaintWars_SteeleGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APaintWars_SteeleGameMode(Z_Construct_UClass_APaintWars_SteeleGameMode, &APaintWars_SteeleGameMode::StaticClass, TEXT("/Script/PaintWars_Steele"), TEXT("APaintWars_SteeleGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APaintWars_SteeleGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
