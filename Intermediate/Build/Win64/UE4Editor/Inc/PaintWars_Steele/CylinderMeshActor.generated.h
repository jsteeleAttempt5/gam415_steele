// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTWARS_STEELE_CylinderMeshActor_generated_h
#error "CylinderMeshActor.generated.h already included, missing '#pragma once' in CylinderMeshActor.h"
#endif
#define PAINTWARS_STEELE_CylinderMeshActor_generated_h

#define PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_SPARSE_DATA
#define PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_RPC_WRAPPERS
#define PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACylinderMeshActor(); \
	friend struct Z_Construct_UClass_ACylinderMeshActor_Statics; \
public: \
	DECLARE_CLASS(ACylinderMeshActor, ARuntimeMeshActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWars_Steele"), NO_API) \
	DECLARE_SERIALIZER(ACylinderMeshActor)


#define PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_INCLASS \
private: \
	static void StaticRegisterNativesACylinderMeshActor(); \
	friend struct Z_Construct_UClass_ACylinderMeshActor_Statics; \
public: \
	DECLARE_CLASS(ACylinderMeshActor, ARuntimeMeshActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintWars_Steele"), NO_API) \
	DECLARE_SERIALIZER(ACylinderMeshActor)


#define PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACylinderMeshActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACylinderMeshActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACylinderMeshActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACylinderMeshActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACylinderMeshActor(ACylinderMeshActor&&); \
	NO_API ACylinderMeshActor(const ACylinderMeshActor&); \
public:


#define PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACylinderMeshActor(ACylinderMeshActor&&); \
	NO_API ACylinderMeshActor(const ACylinderMeshActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACylinderMeshActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACylinderMeshActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACylinderMeshActor)


#define PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_PRIVATE_PROPERTY_OFFSET
#define PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_8_PROLOG
#define PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_PRIVATE_PROPERTY_OFFSET \
	PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_SPARSE_DATA \
	PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_RPC_WRAPPERS \
	PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_INCLASS \
	PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_PRIVATE_PROPERTY_OFFSET \
	PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_SPARSE_DATA \
	PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_INCLASS_NO_PURE_DECLS \
	PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTWARS_STEELE_API UClass* StaticClass<class ACylinderMeshActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PaintWars_Steele_Source_PaintWars_Steele_CylinderMeshActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
