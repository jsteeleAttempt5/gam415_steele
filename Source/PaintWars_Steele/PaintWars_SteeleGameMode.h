// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PaintWars_SteeleGameMode.generated.h"

UCLASS(minimalapi)
class APaintWars_SteeleGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APaintWars_SteeleGameMode();
};



