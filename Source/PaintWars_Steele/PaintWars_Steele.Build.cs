// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PaintWars_Steele : ModuleRules
{
	public PaintWars_Steele(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay",
            "ProceduralMeshComponent", "RuntimeMeshComponent", "RHI", "RenderCore" });
    }
}
